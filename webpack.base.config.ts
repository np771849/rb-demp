import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { Configuration } from 'webpack';
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import ESLintPlugin from 'eslint-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
// TODO: adopt to plugin tsconfig-paths-webpack-plugin to load alias

const config: Configuration = {
  entry: path.resolve(__dirname, 'frontend/src/index.tsx'),
  module: {
    rules: [
      {
        exclude: /node_modules/,
        test: /\.(ts|js)x?$/i,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react', '@babel/preset-typescript']
          }
        }
      },
      {
        exclude: /node_modules/,
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'frontend/public/index.html')
    }),
    new ForkTsCheckerWebpackPlugin({
      async: false
    }),
    new ESLintPlugin({
      extensions: ['js', 'jsx', 'ts', 'tsx']
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: 'frontend/src/static',
          to: 'static'
        }
      ]
    })
  ],
  resolve: {
    alias: {
      components: path.resolve(__dirname, 'frontend/src/components'),
      hooks: path.resolve(__dirname, 'frontend/src/hooks'),
      mocks: path.resolve(__dirname, 'frontend/__mocks__'),
      types: path.resolve(__dirname, 'frontend/src/types'),
      utils: path.resolve(__dirname, 'frontend/src/utils')
    },
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json', '.css', '.scss'],
    modules: ['node_modules', path.join(__dirname, 'frontend/src')]
  }
};

export default config;
