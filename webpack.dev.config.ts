import merge from 'webpack-merge';
import path from 'path';
import webpack, { Configuration as WebpackConfiguration } from 'webpack';
import { Configuration as WebpackDevServerConfiguration } from 'webpack-dev-server';
import baseWebpackConfig from './webpack.base.config';

interface Configuration extends WebpackConfiguration {
  devServer?: WebpackDevServerConfiguration;
}

const config: Configuration = merge<Configuration>(baseWebpackConfig, {
  devServer: {
    contentBase: path.resolve(__dirname, 'frontend/dist/dev'),
    historyApiFallback: true,
    hot: true,
    open: true,
    port: 4000
  },
  devtool: 'source-map',
  mode: 'development',
  output: {
    chunkFilename: '[name].js',
    filename: '[name].js',
    path: path.resolve(__dirname, 'frontend/dist/dev')
  },
  plugins: [new webpack.HotModuleReplacementPlugin()]
});

export default config;
