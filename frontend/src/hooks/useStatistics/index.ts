import { useEffect, useReducer, useCallback } from 'react';
import { transformCountryStateResponse } from 'utils/transformers';
import { IOption, ActionType, IStatisticsState, IStatisticsData, ICaronaDataAttribure } from 'types';
import { getStatistics } from 'hooks/useStatistics/helper';
import { statisticsReducer } from 'hooks/useStatistics/reducer';

const initialState = {
  cases: 0,
  deaths: 0,
  error: undefined,
  loading: false,
  recovered: 0
};

const totalReducer =
  (key: keyof Partial<ICaronaDataAttribure>) =>
  (acc: number, current: IStatisticsData = { [key]: 0 }): number =>
    (current?.[key] || 0) + acc;

function useStatistics(region: IOption, weeks: IOption, reducer = statisticsReducer): IStatisticsState {
  const [state, dispatch] = useReducer(reducer, initialState);
  const fetchStatistics = useCallback(async (): Promise<void> => {
    try {
      dispatch({ type: ActionType.FETCH });
      const [cases, deaths, recovered] = await getStatistics(region, weeks);
      dispatch({
        payload: {
          cases: transformCountryStateResponse(cases, region.value).reduce(totalReducer('cases'), 0),
          deaths: transformCountryStateResponse(deaths, region.value).reduce(totalReducer('deaths'), 0),
          recovered: transformCountryStateResponse(recovered, region.value).reduce(totalReducer('recovered'), 0)
        },
        type: ActionType.SUCCESS
      });
    } catch (err) {
      dispatch({
        payload: {
          error: err.message
        },
        type: ActionType.ERROR
      });
    }
  }, [region, weeks]);

  useEffect(() => {
    fetchStatistics();
  }, [fetchStatistics]);

  return state;
}

export default useStatistics;
