import { ActionType, IStatisticsState, IStatisticsReducerAction } from 'types';

export const statisticsReducer = (state: IStatisticsState, action: IStatisticsReducerAction): IStatisticsState => {
  switch (action.type) {
    case ActionType.FETCH: {
      return {
        ...state,
        error: undefined,
        loading: true
      };
    }
    case ActionType.SUCCESS: {
      return {
        ...state,
        ...action.payload,
        error: undefined,
        loading: false
      };
    }
    case ActionType.ERROR: {
      return {
        ...state,
        error: action?.payload?.error,
        loading: false
      };
    }
    default: {
      throw new Error(`Unhandled type: ${action.type}`);
    }
  }
};
