import Api from 'utils/api';
import { HTTP_ROUTES } from 'utils/constants';
import { FilterType, IAPIResponse, ICaronaStateData, IOption, IStatisticsData } from 'types';

const { CASES, DEATHS, RECOVERED } = HTTP_ROUTES;

const getBaseRoute = (region: IOption): string => {
  const { value, type } = region;
  const base = type === FilterType.STATE ? HTTP_ROUTES.STATE : HTTP_ROUTES.COUNTRY;
  return `${base}${value}`;
};

export const getRoute = (route: string, days: string, region: IOption): string =>
  `${getBaseRoute(region)}${route}${days}`;

export const getData = (
  route: string,
  weeks: IOption,
  region: IOption
): Promise<IAPIResponse<ICaronaStateData | IStatisticsData[]>> =>
  Api.get<ICaronaStateData | IStatisticsData[]>(getRoute(route, weeks.value, region));

export const getStatistics = (
  region: IOption,
  weeks: IOption
): Promise<IAPIResponse<ICaronaStateData | IStatisticsData[]>[]> =>
  Promise.all([getData(CASES, weeks, region), getData(DEATHS, weeks, region), getData(RECOVERED, weeks, region)]);
