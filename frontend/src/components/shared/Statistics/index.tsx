import React, { FC, ReactElement, useState } from 'react';
import Results from 'components/shared/Results';
import Select from 'components/shared/Select';
import Spinner from 'components/shared/Spinner';
import { REGIONS, DATA_RANGE, NAMESPACE } from 'utils/constants';
import useStatistics from 'hooks/useStatistics';
import { IOption } from 'types';
import { useTranslation } from 'react-i18next';
import './index.css';

const Statistics: FC = (): ReactElement => {
  const { t } = useTranslation(NAMESPACE);
  const [region, setRegion] = useState<IOption>(REGIONS[0]);
  const [weeks, setWeeks] = useState<IOption>(DATA_RANGE[0]);
  const state = useStatistics(region, weeks);

  return (
    <div className="container">
      {state.loading && <Spinner />}
      <div className="header">{t('title')}</div>
      <div className="search-options">
        <Select onSelect={option => void setWeeks(option)} options={DATA_RANGE} selected={weeks} />
        <Select onSelect={option => void setRegion(option)} options={REGIONS} selected={region} />
      </div>
      <Results cases={state?.cases} deaths={state?.deaths} recovered={state?.recovered} />
      {state.error && <div className="error"> {state.error} </div>}
    </div>
  );
};

export default Statistics;
