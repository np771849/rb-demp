import React, { FC, ReactElement } from 'react';
import './index.css';

const Spinner: FC = (): ReactElement => (
  <div className="spinner">
    <div className="lds-ring">
      <div />
      <div />
      <div />
      <div />
    </div>
  </div>
);

export default Spinner;
