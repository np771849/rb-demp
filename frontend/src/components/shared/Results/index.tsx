import React, { FC, ReactElement } from 'react';
import { IResultsProps, IResultItemProps } from 'types';
import { useTranslation } from 'react-i18next';
import { NAMESPACE } from 'utils/constants';
import './index.css';

const Result: FC<IResultItemProps> = ({ label, value }): ReactElement => (
  <div className="result">
    <label>{label}</label>
    <label>{value}</label>
  </div>
);

const Results: FC<IResultsProps> = ({ cases, deaths, recovered }): ReactElement => {
  const { t } = useTranslation(NAMESPACE);

  return (
    <div className="results">
      <Result label={t('statistics.cases')} value={cases} />
      <Result label={t('statistics.deaths')} value={deaths} />
      <Result label={t('statistics.recovered')} value={recovered} />
    </div>
  );
};

export default Results;
