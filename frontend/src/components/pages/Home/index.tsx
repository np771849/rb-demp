import React, { FC, ReactElement, Suspense } from 'react';
import Spinner from 'components/shared/Spinner';
import Statistics from 'components/shared/Statistics';

const Home: FC = (): ReactElement => (
  <Suspense fallback={<Spinner />}>
    <Statistics />
  </Suspense>
);

export default Home;
