import { HOST } from 'utils/constants';
import { IAPIResponse, IProcessRequest } from 'types';

export const getUrlPath = (route: string): string => `${HOST}${route}`;

export const processRequest = async <T>({ url, method, headers, body }: IProcessRequest): Promise<IAPIResponse<T>> => {
  try {
    const response = await fetch(url, {
      body,
      headers,
      method
    });
    const result = await response.json();

    if (response.ok) {
      return Promise.resolve(result);
    }

    return Promise.reject(result);
  } catch (error) {
    throw new Error(error);
  }
};

const Api = {
  async get<T>(route: string): Promise<IAPIResponse<T>> {
    const url = getUrlPath(route);

    return processRequest({
      method: 'GET',
      url
    });
  }
};

export default Api;
