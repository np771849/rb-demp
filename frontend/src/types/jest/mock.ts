export interface ISuccessResponse<T> {
  json: () => Promise<T>;
  ok: boolean;
  status: number;
}

export interface IMockFetch {
  [name: string]: unknown;
}

export interface I18NextTMock {
  t: (key: string) => string;
}

export interface I18NextMock {
  useTranslation: () => I18NextTMock;
}
