export interface IResultsProps {
  cases: number;
  deaths: number;
  recovered: number;
}

export interface IResultItemProps {
  label: string;
  value: number;
}
