export interface IAPIResponse<T> {
  error?: string;
  status?: string;
  data?: T;
}

export interface IProcessRequest {
  body?: string;
  headers?: HeadersInit;
  method: string;
  url: string;
}
