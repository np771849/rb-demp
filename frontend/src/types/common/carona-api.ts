import { ActionType } from './data-options';

export interface ICaronaDataAttribure {
  cases: number;
  deaths: number;
  recovered: number;
}

export interface IStatisticsState extends ICaronaDataAttribure {
  loading: boolean;
  error?: string;
}

export interface IStatisticsReducerAction {
  type: ActionType;
  payload?: Partial<IStatisticsState>;
}

export type IStatisticsData = {
  [key in keyof Partial<ICaronaDataAttribure>]: ICaronaDataAttribure[key];
};

export interface ICaronaHistoryData {
  history: IStatisticsData[];
}

export interface ICaronaStateData {
  [key: string]: ICaronaHistoryData;
}
