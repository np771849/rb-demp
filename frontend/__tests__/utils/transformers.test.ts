import { transformCountryStateResponse } from 'utils/transformers';
import countryCasesApiResponse from 'mocks/country-cases-api-response.mock.json';
import statesCasesApiResponse from 'mocks/states-cases-api-response.mock.json';

describe('transformers test', () => {
  it('should return data from the country response', () => {
    const transformed = transformCountryStateResponse(countryCasesApiResponse, '');
    expect(countryCasesApiResponse.data).toMatchObject(transformed);
  });

  it('should return data from the state response', async () => {
    const region = 'HH';
    const transformed = transformCountryStateResponse(statesCasesApiResponse, region);
    expect(statesCasesApiResponse.data[region].history).toMatchObject(transformed);
  });
});
