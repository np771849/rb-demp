import Api, { getUrlPath } from 'utils/api';
import apiResponseMock from 'mocks/country-cases-api-response.mock.json';
import { mockFetch, failureResponse, successResponse, errorResponse } from '../helpers';

describe('Api helper test', () => {
  const route = '/carona/data';
  const url = getUrlPath(route);

  it('should return the api response ', async () => {
    mockFetch({ [url]: successResponse(apiResponseMock) });
    const res = await Api.get(route);
    expect(res).toMatchObject(apiResponseMock);
  });

  it('should return error when status code is not 200', async () => {
    mockFetch({ [url]: failureResponse(403, 'Error') });
    expect(Api.get(route)).rejects.toMatch('Error');
  });

  it('should return error ', async () => {
    mockFetch({ [url]: errorResponse('Error') });
    Api.get(route).catch(e => expect(e.message).toMatch('Error'));
  });
});
