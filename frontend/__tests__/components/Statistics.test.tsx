import Statistics from 'components/shared/Statistics';
import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import * as useStatisticsHelper from 'hooks/useStatistics/helper';
import { DATA_RANGE, REGIONS } from 'utils/constants';

const getStatistics = jest.spyOn(useStatisticsHelper, 'getStatistics');

describe('<Statistics />', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render', async () => {
    render(<Statistics />);
    expect(getStatistics).toBeCalled();
    expect(await screen.findByText(/Cases/i)).toBeTruthy();
  });

  it('should call the api to get statistics on time change', async () => {
    const { findByText, getAllByTestId } = render(<Statistics />);
    fireEvent.change((await getAllByTestId('select'))[0], { target: { value: DATA_RANGE[1].value } });
    expect(getStatistics).toHaveBeenCalledWith(REGIONS[0], DATA_RANGE[1]);
    expect(await findByText(DATA_RANGE[1].text)).toBeTruthy();
  });

  it('should call the api to get statistics on region change', async () => {
    const { findByText, getAllByTestId } = render(<Statistics />);
    fireEvent.change((await getAllByTestId('select'))[1], { target: { value: REGIONS[1].id } });
    expect(getStatistics).toHaveBeenCalledWith(REGIONS[1], DATA_RANGE[0]);
    expect(await findByText(REGIONS[1].text)).toBeTruthy();
  });

  it('should show the error', async () => {
    const errorMessage = 'Something went wrong';
    getStatistics.mockRejectedValueOnce({ message: errorMessage });
    const { findByText } = render(<Statistics />);
    expect(await findByText(errorMessage)).toBeTruthy();
  });
});
