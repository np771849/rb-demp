import { ISuccessResponse, IMockFetch } from 'types';

const sendResponse = <T>(ok: boolean, status: number, data: T): Promise<ISuccessResponse<T>> =>
  Promise.resolve({
    json: () => Promise.resolve(data),
    ok,
    status
  });

export const successResponse = <T>(data: T): Promise<ISuccessResponse<T>> => {
  const mockJson = Promise.resolve(data);
  return Promise.resolve({
    json: () => mockJson,
    ok: true,
    status: 200
  });
};

export const failureResponse = <T>(status: number, errorData: T): Promise<ISuccessResponse<T>> =>
  sendResponse(false, status, errorData);

export const errorResponse = <T>(error: T): Promise<T> => Promise.reject(error);

export const mockFetch = (paths: IMockFetch): void => {
  global.fetch = jest.fn().mockImplementation(url => paths[url]);
};
