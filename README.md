## Demo web application

**Problem statement**

_Create a web application to show recent Corona statistics about Germany. The data should be queried from a public accessible REST API.  
Two interactive elements are requested:_

- _a selection of a region: These regions are Germany in total or a single county such as BW, RP, etc._
- _a time frame for the statistics: 1,2,3,4 weeks back from now  
  The statistics should cover: total number of cases, total number of deaths and total number of recovered. Each for the selected period of time (e.g. 2 weeks back from now)._

**Tech stack**

React, Typescript,
[Corona statistics API](https://api.corona-zahlen.org)

**Tooling**

- Bundeling
  - webpack
- Lint
  - eslint
  - stylelint
  - prettier
- Localization
  - i18next
- Testing
  - jest
  - react-testing-library
- Pre commit hooks
  - husky
- CI / CD
  - gitLab
- Deployment
  - surge

**How to run application**
_Go to the root directory and execute the below command_

```
npm install
npm run start
```

**Pipeline and deployment flow**

- **master branch** - for master the pipeline looks like below as soon as the code is merged to master it will build, test, and deploy the code to integration environment and deploy to production is a manual step. Environment url [Prod](http://np77.surge.sh).

```mermaid
graph LR
A((Install)) ---> B((Test)) ---> C((Build)) -- automatic deploy to dev ---> D((deploy))
C -- manual deploy to prod ---> E((deploy)) ---> F((Smoke))
```

- **develop branch** - for develop the pipeline looks like below as soon as the code is merged to develop it will build, test, and deploy the code to integration environment. Environment url [Dev](http://np77-dev.surge.sh).

```mermaid
graph LR
A((Install)) ---> B((Test)) ----> C((Build)) -- deploy to dev ---> D((Deploy))
```

- **feature branch** - for feature branches the pipeline looks like below here it will have and extra step 'remove-feature' to remove the application so our systems will not go out of memory it will be executed automatically when the merge request is merged and will remove the app.
  The environment url will be `http://np77-<branch-name>.surge.sh`. for example of the branch name is feature/hello then it can be accessed at `http://np77-feature-hello.surge.sh`

```mermaid
graph LR
A((Install)) ----> B((Test)) ---> C((Build))  -- automatic ---> D((Deploy))
C -- on merge ---> E((Remove))
```
