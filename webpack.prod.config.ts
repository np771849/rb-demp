import merge from 'webpack-merge';
import path from 'path';
import { Configuration } from 'webpack';
import baseWebpackConfig from './webpack.base.config';

const config: Configuration = merge<Configuration>(baseWebpackConfig, {
  mode: 'production',
  output: {
    chunkFilename: '[name].[hash].js',
    filename: '[name].[hash].js',
    path: path.resolve(__dirname, 'frontend/dist/prod')
  }
});

export default config;
